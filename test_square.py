import unittest

from cuadrado import Cuadrado


class Test_square(unittest.TestCase):
    cua = None

    def setUp(self) -> None:
        self.cua = Cuadrado(5)

    def test_instance(self):
        self.assertNotEqual(self.cua, None)

    def test_square_area(self):
        self.assertEqual(self.cua.calcula_area(), 25)

    def test_square_per(self):
        self.assertEqual(self.cua.calcula_perimetro(), 20)


if __name__ == '__main__':
    unittest.main()
