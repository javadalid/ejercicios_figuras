import unittest

from circulo import Circulo


class Test_circle(unittest.TestCase):
    cir = None

    def setUp(self) -> None:
        self.cir = Circulo(10)

    def test_instance(self):
        self.assertNotEqual(self.cir, None)

    def test_circle_area(self):
        self.assertEqual(round(self.cir.calcula_area()), 314)

    def test_circle_per(self):
        self.assertEqual(round(self.cir.calcula_perimetro()), 63)


if __name__ == '__main__':
    unittest.main()
