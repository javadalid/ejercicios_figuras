from abc import ABCMeta, abstractmethod


class Shape:
    __metaclass__ = ABCMeta

    @abstractmethod
    def calcula_area(self):
        raise NotImplementedError

    @abstractmethod
    def calcula_perimetro(self):
        raise NotImplementedError
