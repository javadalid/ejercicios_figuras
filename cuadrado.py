from shape import Shape


class Cuadrado(Shape):
    lado = 0

    def __init__(self, lado):
        self.lado = lado

    def calcula_area(self):
        return self.lado * self.lado

    def calcula_perimetro(self):
        return self.lado * 4
