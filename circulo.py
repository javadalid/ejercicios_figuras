from math import pi
from shape import Shape


class Circulo(Shape):
    radio = 0

    def __init__(self, radio):
        self.radio = radio

    def calcula_area(self):
        return pi * self.radio * self.radio

    def calcula_perimetro(self):
        return 2 * (pi * self.radio)
