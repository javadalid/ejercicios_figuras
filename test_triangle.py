import unittest

from triangulo import Triangulo


class Test_triangle(unittest.TestCase):
    tri = None

    def setUp(self) -> None:
        self.tri = Triangulo(6, 8)

    def test_instance(self):
        self.assertNotEqual(self.tri, None)

    def test_triangle_area(self):
        self.assertEqual(self.tri.calcula_area(), 24)

    def test_triangle_per(self):
        self.assertEqual(self.tri.calcula_perimetro(), 18)


if __name__ == '__main__':
    unittest.main()
